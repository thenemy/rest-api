const db = require('../modules/employees')
const MemberModel = require('../models/MemberModel')

module.exports = {
    get_all_members: function(req, res){
        MemberModel.get_all_members((result) => {
            res.json(result);
        })
    }
}