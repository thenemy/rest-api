const express = require('express');

const app = express()

const user_router = require('./user');

app.use('/api', user_router)

app.listen(3000, () => {
    console.log("Listening on http://localhost:3000");
})